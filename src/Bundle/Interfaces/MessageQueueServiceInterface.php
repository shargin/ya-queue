<?php

namespace Akson\Bundle\SQSQueue\Interfaces;

use Akson\Bundle\SQSQueue\Model\IncomingQueueMessage;
use Akson\Bundle\SQSQueue\Model\OutgoingQueueMessage;

interface MessageQueueServiceInterface
{
    /**
     * @param OutgoingQueueMessage $message
     * @return string|null
     */
    public function publishMessage(OutgoingQueueMessage $message): ?string;

    /**
     * @param array $messages
     * @return int
     */
    public function publishMessageBatch(array $messages): int;

    /**
     * @param int $count
     * @return IncomingQueueMessage[]
     */
    public function receiveMessages(int $count): array;

    /**
     * @param IncomingQueueMessage $message
     * @return bool
     */
    public function deleteMessage(IncomingQueueMessage $message): bool;
}